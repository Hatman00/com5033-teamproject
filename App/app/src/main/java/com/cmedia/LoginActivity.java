package com.cmedia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    DBHandler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        handler = new DBHandler(this);
        SQLiteDatabase database = handler.getWritableDatabase();
        handler.onCreate(database);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        String user = sp.getString("username", "na");
        Log.d("LoginActivityOnCreate", "SharedPreferences user is " + user);
        if (handler.checkUserExists(user, null, false)){
            Intent loginSwitch = new Intent(this, HomeActivity.class);
            loginSwitch.putExtra("username", user);
            loginSwitch.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(loginSwitch);
            finish();
        }
    }

    public void loginButton(View view){
        EditText user = findViewById(R.id.app_signin_textbox_user);
        EditText pass = findViewById(R.id.app_signin_textbox_password);
        String userString = String.valueOf(user.getText());
        String passString = String.valueOf(pass.getText());
        Boolean loginSuccess = false;
        Log.d("loginButton", "User pressed the login button");
        Log.d("loginButton", "userString is " + userString + "\npassString is " + passString);
        if (userString.equals("")){
            Log.d("loginButton", "Username is empty");
            user.setError(getResources().getString(R.string.app_error_nosigninuser));
            user.requestFocus();
            return;
        }
        else if (passString.equals("")){
            Log.d("loginButton", "Password is empty");
            pass.setError(getResources().getString(R.string.app_error_nosigninpasss));
            pass.requestFocus();
            return;
        }
        else{
            Log.d("loginButton", "Checking if user exists");
            if (handler.checkUserExists(userString, passString, false)){
                Log.d("loginButton", "User with username exists");
                Toast.makeText(this, getResources().getString(R.string.app_misc_loggedin), Toast.LENGTH_LONG).show();
                loginSuccess = true;
            }
            else if (handler.checkUserExists(userString, passString, true)){
                Log.d("loginButton", "User with email exists");
                Toast.makeText(this, getResources().getString(R.string.app_misc_loggedin), Toast.LENGTH_LONG).show();
                userString = handler.getUsername(userString);
                Log.d("loginButton", "Changed userString to " + userString);
                loginSuccess = true;
            }
            else{
                Log.d("loginButton", "User does NOT exist");
                Toast.makeText(this, getResources().getString(R.string.app_error_loginfail), Toast.LENGTH_LONG).show();
            }
        }
        if (loginSuccess){
            Log.d("loginButton", "Login successful");
            InputMethodManager imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            Intent homeSwitch = new Intent(this, HomeActivity.class);
            homeSwitch.putExtra("username", userString);
            homeSwitch.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(homeSwitch);
            finish();
        }
    }

    public void signupButton(View view){
        Intent signupSwithch = new Intent(this, sighnupActivity.class);
        startActivity(signupSwithch);
    }

    @Override
    public void onStop(){
        super.onStop();
        Log.d("LoginActivityOnStop", "Activity has stopped");
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        sp.edit().clear().commit();
        Log.d("LoginActivityOnStop", "SharedPreferences has been cleared");
        String user = sp.getString("username", "na");
        Log.d("LoginActivityOnStop", user);
    }
}