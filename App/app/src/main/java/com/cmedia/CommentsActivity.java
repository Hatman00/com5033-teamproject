package com.cmedia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class CommentsActivity extends AppCompatActivity {
    String username;
    String videoID;
    DBHandler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        Intent usernameIntent = getIntent();
        username = usernameIntent.getStringExtra("username");
        videoID = usernameIntent.getStringExtra("videoID");
        Log.d("CommentsActivityOnCreate","username is "+username);
        handler = new DBHandler(this);
        SQLiteDatabase database = handler.getWritableDatabase();
        handler.onCreate(database);
    }
    public void backButton(View view){
        Intent videoSwitch = new Intent(this, VideoActivity.class);
        videoSwitch.putExtra("username", username);
        videoSwitch.putExtra("videoID", videoID);
        startActivity(videoSwitch);
        finish();
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Intent videoSwitch = new Intent(this, VideoActivity.class);
        videoSwitch.putExtra("username", username);
        videoSwitch.putExtra("videoID", videoID);
        startActivity(videoSwitch);
        finish();
    }
}