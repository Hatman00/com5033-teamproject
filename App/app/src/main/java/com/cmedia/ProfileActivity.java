package com.cmedia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ProfileActivity extends AppCompatActivity {

    String username;
    DBHandler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Intent usernameIntent = getIntent();
        username = usernameIntent.getStringExtra("username");
        Log.d("ProfileActivityOnCreate", "username is " + username);
        handler = new DBHandler(this);
        SQLiteDatabase database = handler.getWritableDatabase();
        handler.onCreate(database);
        loadPFP();
    }

    public void loadPFP(){
        Log.d("loadPFP", "Getting user profile pic");
        String userID = handler.getUserID(username);
        Log.d("loadPFP", "userID is " + userID);
        ImageView pfp = findViewById(R.id.app_profile_imageView_userPFP);
        pfp.setImageURI(Uri.parse("android.resource://" + getPackageName() + "/" + getResources().getIdentifier("u" + userID, "drawable", getPackageName())));
        if (pfp.getDrawable() == null){
            Log.d("loadPFP", "PFP not found, using default");
            pfp.setImageURI(Uri.parse("android.resource://" + getPackageName() + "/" + getResources().getIdentifier("defaultuser", "drawable", getPackageName())));
        }
    }

    public void deleteConfirmation(View view){
        ConstraintLayout mainConstraint = findViewById(R.id.app_profile_constraint_main);
        ConstraintLayout deleteConstraint = findViewById(R.id.app_profile_constraint_deleteconfirm);
        constraintSwitch(view, mainConstraint, deleteConstraint);
    }

    public void deleteAccount(View view){
        handler.deleteUserAccount(username);
        Intent loginSwitch = new Intent(this, LoginActivity.class);
        loginSwitch.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginSwitch);
        finish();
    }

    public void constraintSwitch(View view, ConstraintLayout startConstraint, ConstraintLayout endConstraint){
        Log.d("constraintSwitch", "Swapping constraints");
        if (startConstraint.getVisibility() == View.GONE){
            Log.d("constraintSwitch", "Making start constraint VISIBLE and end constraint GONE");
            startConstraint.setVisibility(View.VISIBLE);
            endConstraint.setVisibility(View.GONE);
        }
        else{
            Log.d("constraintSwitch", "Making start constraint GONE and end constraint VISIBLE");
            startConstraint.setVisibility(View.GONE);
            endConstraint.setVisibility(View.VISIBLE);
        }
    }

    public void backButton(View view){
        Intent homeSwitch = new Intent(this, HomeActivity.class);
        homeSwitch.putExtra("username", username);
        startActivity(homeSwitch);
        finish();
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Intent homeSwitch = new Intent(this, HomeActivity.class);
        homeSwitch.putExtra("username", username);
        startActivity(homeSwitch);
        finish();
    }
}