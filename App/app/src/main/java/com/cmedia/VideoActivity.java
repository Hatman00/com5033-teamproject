package com.cmedia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

public class VideoActivity extends AppCompatActivity {

    String username;
    String videoID;
    DBHandler handler;
    private MediaController mediaController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        Intent usernameIntent = getIntent();
        username = usernameIntent.getStringExtra("username");
        videoID = usernameIntent.getStringExtra("videoID");
        Log.d("VideoActivityOnCreate", "username is " + username);
        Log.d("VideoActivityOnCreate", "videoID is " + videoID);
        handler = new DBHandler(this);
        SQLiteDatabase database = handler.getWritableDatabase();
        handler.onCreate(database);
        setupVideo();
        handler.increaseVideoViewCount(videoID);
    }

    public void setupVideo(){
        String videoInfo = handler.getIndividualVideoInfo(videoID);
        if (!videoInfo.equals("")){
            TextView videoTitle = findViewById(R.id.app_video_textView_videoTitle);
            TextView videoDescription = findViewById(R.id.app_video_textView_videoInfo);
            VideoView videoPlayer = findViewById(R.id.app_video_videoView_video);
            videoTitle.setText(handler.getIndividualVideoName(videoID));
            videoDescription.setText(videoInfo);
            videoPlayer.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + getResources().getIdentifier("v" + videoID, "raw", getPackageName())));
            mediaController = new MediaController(this);
            mediaController.setAnchorView(videoPlayer);
            videoPlayer.setMediaController(mediaController);
            videoPlayer.start();
        }
    }
    public void commentButton(View view){
        Intent commentSwitch = new Intent(this,CommentsActivity.class);
        commentSwitch.putExtra("username",username);
        startActivity(commentSwitch);
    }
    public void backButton(View view){
        Intent homeSwitch = new Intent(this, HomeActivity.class);
        homeSwitch.putExtra("username", username);
        startActivity(homeSwitch);
        finish();
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Intent homeSwitch = new Intent(this, HomeActivity.class);
        homeSwitch.putExtra("username", username);
        startActivity(homeSwitch);
        finish();
    }
}