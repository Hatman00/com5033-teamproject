package com.cmedia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.res.ResourcesCompat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;

public class HomeActivity extends AppCompatActivity {

    String username;
    DBHandler handler;
    Context context;
    boolean loggingOut = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Intent usernameIntent = getIntent();
        username = usernameIntent.getStringExtra("username");
        Log.d("HomeActivityOnCreate", "username is " + username);
        TextView userText = findViewById(R.id.app_home_textView_username);
        userText.setText(username);
        handler = new DBHandler(this);
        SQLiteDatabase database = handler.getWritableDatabase();
        handler.onCreate(database);
        context = this;
        listHomeVideos();
    }

    public void listHomeVideos(){
        Log.d("listHomeVideos", "Listing videos...");
        String[] videoList = handler.listVideos();
        Log.d("listHomeVideos", "Video list received.\nList length is " + videoList.length);
        if (videoList.length > 0){
            LinearLayout videoScroll = findViewById(R.id.app_home_linearlayout_videolist);
            videoScroll.setGravity(Gravity.CENTER);
            String[] videoListSplit = new String[9];
            Log.d("listHomeVideos", "Adding videos to scrollview");
            LinearLayout[] videoLinear = new LinearLayout[videoList.length]; //A list of videos' linear layouts that will contain its thumb and its title as a textview.
            TextView[] videoTitle = new TextView[videoList.length]; //A list of videos' titles as textviews.
            ImageView[] videoImage = new ImageView[videoList.length]; //A list of videos' thumbs as imageviews.
            int x = 0;
            while (x < videoList.length){
                Log.d("listHomeVideos", "X is " + String.valueOf(x));
                Log.d("listHomeVideos", "Current list item is " + videoList[x]);
                videoListSplit = videoList[x].split("/");
                Log.d("listHomeVideos", "videoList has been split");
                videoLinear[x] = new LinearLayout(this);
                videoLinear[x].setGravity(Gravity.CENTER);
                videoLinear[x].setOrientation(LinearLayout.VERTICAL);
                videoTitle[x] = new TextView(this);
                videoImage[x] = new ImageView(this);
                videoTitle[x].setText(videoListSplit[1]);
                videoTitle[x].setTextColor(Color.WHITE);
                videoTitle[x].setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
                videoTitle[x].setGravity(Gravity.CENTER);
                videoLinear[x].addView(videoTitle[x]);
                try{
                    Log.d("listHomeVideos", "Getting video image");
                    videoImage[x].setImageURI(Uri.parse("android.resource://" + getPackageName() + "/" + getResources().getIdentifier("v" + videoListSplit[0], "drawable", getPackageName())));
                    if (videoImage[x].getDrawable() == null){
                        throw new Exception("Video Thumb Not Found");
                    }
                    Log.d("listHomeVideos", "Video image found");
                }
                catch (Exception error) {
                    Log.d("listHomeVideos", "Video image NOT found, using default");
                    videoImage[x].setBackground(AppCompatResources.getDrawable(this, R.drawable.resource_default));
                }
                videoImage[x].setId(Integer.parseInt(videoListSplit[0]));
                videoImage[x].setOnClickListener(videoListener);
                LinearLayout.LayoutParams videoImageParams = new LinearLayout.LayoutParams(944, 500);
                videoImage[x].setLayoutParams(videoImageParams);
                videoLinear[x].addView(videoImage[x]);
                videoScroll.addView(videoLinear[x]);
                x++;
            }
        }
        else{
            Log.d("listHomeVideos", "No videos listed, showing error");
            //Do that thing
        }
    }

    View.OnClickListener videoListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent videoActivitySwitch = new Intent(context, VideoActivity.class);
            videoActivitySwitch.putExtra("username", username);
            videoActivitySwitch.putExtra("videoID", String.valueOf(view.getId()));
            startActivity(videoActivitySwitch);
        }
    };

    public void searchSwitch(View view){
        Intent searchActivitySwitch = new Intent(this, SearchActivity.class);
        searchActivitySwitch.putExtra("username", username);
        startActivity(searchActivitySwitch);
        finish();
    }

    public void uploadButton(View view){
        Intent uploadSwitch = new Intent(this, UploadActivity.class);
        uploadSwitch.putExtra("username", username);
        startActivity(uploadSwitch);
    }

    public void logoutButton(View view){
        loggingOut = true;
        Log.d("logoutButton", "User pressed the logout button");
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        sp.edit().clear().commit();
        Log.d("logoutButton", "Shared preferences cleared");
        Intent logoutSwitch = new Intent(this, LoginActivity.class);
        startActivity(logoutSwitch);
        finish();
    }

    public void profileButton(View view){
        Intent profileSwitch = new Intent(this, ProfileActivity.class);
        profileSwitch.putExtra("username", username);
        startActivity(profileSwitch);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        loggingOut = true;
        Log.d("logoutOnBackPressed", "User pressed their back button");
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        sp.edit().clear().commit();
        Log.d("logoutOnBackPressed", "Shared preferences cleared");
        Intent logoutSwitch = new Intent(this, LoginActivity.class);
        startActivity(logoutSwitch);
        finish();
    }

    @Override
    public void onStop(){
        super.onStop();
        if (!loggingOut) {
            Log.d("HomeActivityOnStop", "Activity has stopped");
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor edit = sp.edit();
            edit.putString("username", username).apply();
        }
    }
}