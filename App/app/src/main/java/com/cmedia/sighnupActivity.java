package com.cmedia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class sighnupActivity extends AppCompatActivity {

    DBHandler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sighnup);
        handler = new DBHandler(this);
        SQLiteDatabase database = handler.getWritableDatabase();
        handler.onCreate(database);
    }

    public void signupButton(View view){
        EditText username = findViewById(R.id.app_signup_textbox_username);
        EditText email = findViewById(R.id.app_signup_textbox_email);
        EditText password = findViewById(R.id.app_signup_textbox_password);
        EditText repass = findViewById(R.id.app_signup_textbox_repass);
        Log.d("signupButton", "Getting user info from text boxes...");
        if (String.valueOf(username.getText()).equals("")){
            Log.d("signupButton", "Username is empty!");
            username.setError(getResources().getString(R.string.app_error_nouser));
            username.requestFocus();
            return;
        }
        else if (String.valueOf(email.getText()).equals("")){
            Log.d("signupButton", "Email is empty!");
            email.setError(getResources().getString(R.string.app_error_noemail));
            email.requestFocus();
            return;
        }
        else if (String.valueOf(password.getText()).equals("") || String.valueOf(password.getText()).length() < 6){
            if (String.valueOf(password.getText()).equals("")){
                Log.d("signupButton", "Password is empty!");
                password.setError(getResources().getString(R.string.app_error_nopass));
            }
            else if (String.valueOf(password.getText()).length() < 6){
                Log.d("signupButton", "Password is too short!");
                password.setError(getResources().getString(R.string.app_error_shortpass));
            }
            password.requestFocus();
            return;
        }
        else if (String.valueOf(repass.getText()).equals("") || !String.valueOf(repass.getText()).equals(String.valueOf(password.getText()))){
            if (String.valueOf(repass.getText()).equals("")){
                Log.d("signupButton", "Re-Enter password is empty!");
                repass.setError(getResources().getString(R.string.app_error_norepass));
                repass.requestFocus();
            }
            else if (!String.valueOf(repass.getText()).equals(String.valueOf(password.getText()))){
                Log.d("signupButton", "Re-Enter password does NOT match password!");
                repass.setError(getResources().getString(R.string.app_error_repassmatch));
                password.setError(getResources().getString(R.string.app_error_repassmatch));
            }
            return;
        }
        else if (handler.checkUserExists(String.valueOf(username.getText()), null, false)){
            Log.d("signupButton", "User with username already exists!");
            Toast.makeText(this, getResources().getString(R.string.app_misc_userexists), Toast.LENGTH_LONG).show();
            return;
        }
        else if (handler.checkUserExists(String.valueOf(email.getText()), null, true)){
            Log.d("signupButton", "User with email already exists!");
            Toast.makeText(this, getResources().getString(R.string.app_misc_emailexists), Toast.LENGTH_LONG).show();
            return;
        }
        else{
            Log.d("signupButton", "User information is okay");
            if (handler.createNewUser(String.valueOf(username.getText()), String.valueOf(password.getText()), String.valueOf(email.getText()), "Shhh...",  false, false)){
                Log.d("signupButton", "Successfully created user");
                Toast.makeText(this, getResources().getString(R.string.app_misc_usercreatesuccess), Toast.LENGTH_LONG).show();
                backButton(view);
            }
            else{
                Log.d("signupButton", "Failed to create user");
                Toast.makeText(this, getResources().getString(R.string.app_misc_usercreatefail), Toast.LENGTH_LONG).show();
            }
        }
    }

    public void backButton(View view){
        InputMethodManager imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        Intent loginSwitch = new Intent(this, LoginActivity.class);
        startActivity(loginSwitch);
        finish();
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Intent loginSwitch = new Intent(this, LoginActivity.class);
        startActivity(loginSwitch);
        finish();
    }
}