package com.cmedia;

import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class UploadActivity extends AppCompatActivity {

    String username;
    DBHandler handler;
    int permission;
    private static final int REQUEST_CODE = 101;
    boolean filesPermitted;
    private static final int SELECT_VIDEO = 1;

    private String selectedVideoPath;
    private MediaController mediaController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
        Intent usernameIntent = getIntent();
        username = usernameIntent.getStringExtra("username");
        Log.d("UploadActivityOnCreate", "username is " + username);
        handler = new DBHandler(this);
        SQLiteDatabase database = handler.getWritableDatabase();
        handler.onCreate(database);
        setupPermissions();
    }

    private void setupPermissions(){
        permission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_VIDEO);
        if (permission != PackageManager.PERMISSION_GRANTED){
            Log.d("setupPermissions", "Permission to read system files denied!");
            filesPermitted = false;
            makeRequest();
        }
        else{
            Log.d("setupPermissions", "Permission to read system files accepted.");
            filesPermitted = true;
        }
    }

    public void makeRequest(){
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_MEDIA_VIDEO}, REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case REQUEST_CODE:
                if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED){
                    Log.d("onRequestPermissionsResult", "Permission denied.");
                    filesPermitted = false;
                }
                else{
                    Log.d("onRequestPermissionsResult", "Permission granted");
                    filesPermitted = true;
                }
        }
    }

    public void uploadButton(View view){
        if (filesPermitted){
            Intent photoPickerIntent = new Intent();
            photoPickerIntent.setType("video/");
            photoPickerIntent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(photoPickerIntent, "Select Video"), SELECT_VIDEO);
        }
        else{
            Log.d("uploadButton", "filesPermitted is false, doing nothing");
            Toast.makeText(this, getResources().getString(R.string.app_error_filepermissiondenied), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            Uri selectedVideoURI = data.getData();
            selectedVideoPath = getPath(selectedVideoURI);
            TextView title = findViewById(R.id.app_upload_textView_sorry);
            Button upload = findViewById(R.id.app_upload_button_uploadFD);
            VideoView video = findViewById(R.id.app_upload_videoView_haveyourcakeandeatittoo);
            title.setVisibility(View.VISIBLE);
            upload.setVisibility(View.GONE);
            video.setVideoURI(selectedVideoURI);
            mediaController = new MediaController(this);
            mediaController.setAnchorView(video);
            video.setMediaController(mediaController);
            video.setVisibility(View.VISIBLE);
            video.start();
        }
    }

    public String getPath(Uri uri) {
        // just some safety built in
        if (uri == null) {
            return null;
        }
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String path = cursor.getString(column_index);
            cursor.close();
            return path;
        }
        return uri.getPath();
    }



    public void backButton(View view){
        Intent homeSwitch = new Intent(this, HomeActivity.class);
        homeSwitch.putExtra("username", username);
        startActivity(homeSwitch);
        finish();
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Intent homeSwitch = new Intent(this, HomeActivity.class);
        homeSwitch.putExtra("username", username);
        startActivity(homeSwitch);
        finish();
    }
}