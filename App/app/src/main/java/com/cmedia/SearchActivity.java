package com.cmedia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class SearchActivity extends AppCompatActivity {

    String username;
    DBHandler handler;
    Context context;
    boolean loggingOut = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Intent usernameIntent = getIntent();
        username = usernameIntent.getStringExtra("username");
        Log.d("SearchActivityOnCreate", "username is " + username);
        TextView userText = findViewById(R.id.app_search_textView_username);
        userText.setText(username);
        handler = new DBHandler(this);
        SQLiteDatabase database = handler.getWritableDatabase();
        handler.onCreate(database);
        context = this;
    }

    public void searchButton(View view){
        Log.d("searchButton", "Searching videos..."); //TODO: FIX THIS FUNCTION AND ALL THAT
        EditText searchBox = findViewById(R.id.app_search_textbox_searchText);
        if (!String.valueOf(searchBox.getText()).equals("")) {
            String searchText = String.valueOf(searchBox.getText());
            Log.d("searchButton", "searchText is " + searchText);
            String[] videoList = handler.videoSearch(searchText);
            Log.d("searchButton", "Video list received from search.\nList length is " + videoList.length);
            LinearLayout videoScroll = findViewById(R.id.app_search_linear_videoScroll);
            if (videoScroll.getChildCount() > 0){
                Log.d("searchButton", "videoScroll has child views, deleting");
                videoScroll.removeAllViews();
            }
            if (videoList.length > 0) {
                videoScroll.setGravity(Gravity.CENTER);
                String[] videoListSplit = new String[9];
                Log.d("searchButton", "Adding videos to scrollview");
                LinearLayout[] videoLinear = new LinearLayout[videoList.length]; //A list of videos' linear layouts that will contain its thumb and its title as a textview.
                TextView[] videoTitle = new TextView[videoList.length]; //A list of videos' titles as textviews.
                ImageView[] videoImage = new ImageView[videoList.length]; //A list of videos' thumbs as imageviews.
                int x = 0;
                while (x < videoList.length) {
                    Log.d("searchButton", "X is " + String.valueOf(x));
                    Log.d("searchButton", "Current list item is " + videoList[x]);
                    videoListSplit = videoList[x].split("/");
                    Log.d("searchButton", "videoList has been split");
                    videoLinear[x] = new LinearLayout(this);
                    videoLinear[x].setGravity(Gravity.CENTER);
                    videoLinear[x].setOrientation(LinearLayout.VERTICAL);
                    videoTitle[x] = new TextView(this);
                    videoImage[x] = new ImageView(this);
                    videoTitle[x].setText(videoListSplit[1]);
                    videoTitle[x].setTextColor(Color.WHITE);
                    videoTitle[x].setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
                    videoTitle[x].setGravity(Gravity.CENTER);
                    videoLinear[x].addView(videoTitle[x]);
                    try {
                        Log.d("searchButton", "Getting video image");
                        videoImage[x].setImageURI(Uri.parse("android.resource://" + getPackageName() + "/" + getResources().getIdentifier("v" + videoListSplit[0], "drawable", getPackageName())));
                        if (videoImage[x].getDrawable() == null) {
                            throw new Exception("Video Thumb Not Found");
                        }
                        Log.d("searchButton", "Video image found");
                    } catch (Exception error) {
                        Log.d("searchButton", "Video image NOT found, using default");
                        videoImage[x].setBackground(AppCompatResources.getDrawable(this, R.drawable.resource_default));
                    }
                    videoImage[x].setId(Integer.parseInt(videoListSplit[0]));
                    videoImage[x].setOnClickListener(videoListener);
                    LinearLayout.LayoutParams videoImageParams = new LinearLayout.LayoutParams(944, 500);
                    videoImage[x].setLayoutParams(videoImageParams);
                    videoLinear[x].addView(videoImage[x]);
                    videoScroll.addView(videoLinear[x]);
                    x++;
                }
            } else {
                Log.d("searchButton", "No videos listed, showing error");
                TextView noResults = new TextView(this);
                noResults.setText(getResources().getString(R.string.app_error_search404));
                noResults.setGravity(Gravity.CENTER);
                noResults.setTextColor(Color.RED);
                noResults.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
                videoScroll.addView(noResults);
            }
            searchSwitch(view);
        }
        else{
            Log.d("searchButton", "searchBox is empty, showing error");
            searchBox.setError(getResources().getString(R.string.app_error_emptysearch));
            searchBox.requestFocus();
            return;
        }
    }

    public void searchSwitch(View view) {
        TextView title = findViewById(R.id.app_search_textView_search);
        EditText searchBox = findViewById(R.id.app_search_textbox_searchText);
        Button searchButton = findViewById(R.id.app_search_button_search);
        Button backButton = findViewById(R.id.app_search_button_backButton);
        Button logoutButton = findViewById(R.id.app_search_button_logout);
        ScrollView videoScroll = findViewById(R.id.app_search_scroll_videoScroll);
        if (searchBox.getVisibility() == View.GONE) {
            title.setText(getResources().getString(R.string.app_misc_menu_search));
            searchBox.setVisibility(View.VISIBLE);
            searchButton.setVisibility(View.VISIBLE);
            backButton.setVisibility(View.GONE);
            logoutButton.setVisibility(View.VISIBLE);
            videoScroll.setVisibility(View.GONE);
        } else {
            title.setText(getResources().getString(R.string.app_search_textView_results));
            searchBox.setVisibility(View.GONE);
            searchButton.setVisibility(View.GONE);
            backButton.setVisibility(View.VISIBLE);
            logoutButton.setVisibility(View.INVISIBLE);
            videoScroll.setVisibility(View.VISIBLE);
        }
    }

    View.OnClickListener videoListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent videoActivitySwitch = new Intent(context, VideoActivity.class);
            videoActivitySwitch.putExtra("username", username);
            videoActivitySwitch.putExtra("videoID", String.valueOf(view.getId()));
            startActivity(videoActivitySwitch);
        }
    };

    public void homeButton(View view){
        Intent homeSwitch = new Intent(this, HomeActivity.class);
        homeSwitch.putExtra("username", username);
        startActivity(homeSwitch);
        finish();
    }

    public void logoutButton(View view){
        loggingOut = true;
        Log.d("logoutButton", "User pressed the logout button");
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        sp.edit().clear().commit();
        Log.d("logoutButton", "Shared preferences cleared");
        Intent logoutSwitch = new Intent(this, LoginActivity.class);
        startActivity(logoutSwitch);
        finish();
    }

    public void profileButton(View view){
        Intent profileSwitch = new Intent(this, ProfileActivity.class);
        profileSwitch.putExtra("username", username);
        startActivity(profileSwitch);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        loggingOut = true;
        Log.d("logoutOnBackPressed", "User pressed their back button");
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        sp.edit().clear().commit();
        Log.d("logoutOnBackPressed", "Shared preferences cleared");
        Intent logoutSwitch = new Intent(this, LoginActivity.class);
        startActivity(logoutSwitch);
        finish();
    }

    @Override
    public void onStop(){
        super.onStop();
        if (!loggingOut) {
            Log.d("HomeActivityOnStop", "Activity has stopped");
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor edit = sp.edit();
            edit.putString("username", username).apply();
        }
    }
}