package com.cmedia;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHandler extends SQLiteOpenHelper {
    static SQLiteDatabase db;
    String uID; //A string that will hold the user's ID.

    //Here are the strings used to create the database
    private static final String createTableUsers = "CREATE TABLE IF NOT EXISTS table_users" +
            "(user_id integer PRIMARY KEY NOT NULL," +
            "user_name text NOT NULL," +
            "user_pass text NOT NULL," +
            "user_email text NOT NULL," +
            "user_bio text NOT NULL," +
            "user_private bool NOT NULL," +
            "user_recmes bool NOT NULL," +
            "user_pfp_file text NOT NULL);";

    private static final String createTableVideos = "CREATE TABLE IF NOT EXISTS table_videos" +
            "(video_id integer PRIMARY KEY NOT NULL," +
            "video_name text NOT NULL," +
            "video_desc text NOT NULL," +
            "video_thumb_file text NOT NULL," +
            "video_date text NOT NULL," +
            "video_views integer NOT NULL," +
            "video_likes integer NOT NULL," +
            "video_dislikes integer NOT NULL," +
            "video_file text NOT NULL);";

    private static final String createTableMessages = "CREATE TABLE IF NOT EXISTS table_messages" +
            "(mess_id integer PRIMARY KEY NOT NULL," +
            "mess_contents text NOT NULL," +
            "mess_date text NOT NULL);";

    private static final String createLinkVideoUser = "CREATE TABLE IF NOT EXISTS link_video_user" +
            "(video_id integer NOT NULL," +
            "user_id integer NOT NULL," +
            "PRIMARY KEY(video_id, user_id)," +
            "FOREIGN KEY(video_id) REFERENCES table_videos(video_id)," +
            "FOREIGN KEY(user_id) REFERENCES table_users(user_id));";

    private static final String createLinkMessageUser = "CREATE TABLE IF NOT EXISTS link_message_user" +
            "(mess_id integer NOT NULL," +
            "user_id integer NOT NULL," +
            "PRIMARY KEY(mess_id, user_id)," +
            "FOREIGN KEY(mess_id) REFERENCES table_messages(mess_id)," +
            "FOREIGN KEY(user_id) REFERENCES table_users(user_id));";

    private static final String createLinkUsers = "CREATE TABLE IF NOT EXISTS link_users" +
            "(sub_id integer NOT NULL," +
            "user_id integer NOT NULL," +
            "PRIMARY KEY(sub_id, user_id)," +
            "FOREIGN KEY(sub_id) REFERENCES table_users(user_id)," +
            "FOREIGN KEY(user_id) REFERENCES table_users(user_id));";

    public DBHandler(Context context){
        super(context, "VideoApp.db", null, 1); //This defines what the database file is called. Ask Google or Antesar for more on this because I do not know - Harry
    }

    private String characterEscape(String text){
        if (text.contains("'")){
            StringBuilder textEscape = new StringBuilder(text);
            int escapeIndex = textEscape.indexOf("'");
            textEscape.insert(escapeIndex, "'");
            text = String.valueOf(textEscape);
        }
        if (text.contains("\"")){
            StringBuilder textEscape = new StringBuilder();
            int escapeIndex = textEscape.indexOf("\"");
            textEscape.insert(escapeIndex, "\"");
            text = String.valueOf(textEscape);
        }
        return text;
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        //This is where the tables of the database are added.
        db.execSQL(createTableUsers);
        db.execSQL(createTableVideos);
        db.execSQL(createTableMessages);
        db.execSQL(createLinkVideoUser);
        db.execSQL(createLinkMessageUser);
        db.execSQL(createLinkUsers);
        this.db = db;
        populateDB();
    }

    //The following methods relate to the user and/or their account
    public boolean createNewUser(String userName, String userPassword, String userEmail, String userBio, Boolean userPrivate, Boolean userRecMes){
        /*This adds a new user to the database and returns true.
        If this fails for any reason, it will output the error and return false*/
        Log.d("createNewUser", "Creating new user");
        try {
            ContentValues userValues = new ContentValues();
            userValues.put("user_name", userName);
            userValues.put("user_pass", userPassword);
            userValues.put("user_email", userEmail);
            userValues.put("user_bio", userBio);
            userValues.put("user_private", userPrivate);
            userValues.put("user_recmes", userRecMes);
            userValues.put("user_pfp_file", "None");
            db.insert("table_users", null, userValues);
            return true;
        }
        catch (Exception error){
            Log.d("createNewUser", "Error creating new user");
            Log.d("createNewUser", String.valueOf(error));
            return false;
        }
    }

    public boolean checkUserExists(String username, String password, boolean email) {
        /*This method checks if the user exists.
        If the password IS NOT NULL, this method will check if the user exists AND if their password matches.
        If the password IS NULL, this method will instead only check if the user exists*/
        String query;
        String currentCursor;
        boolean userExists = false;
        Log.d("checkUserExists", "username is " + username + "\npassword is " + String.valueOf(password) + "\nemail is " + String.valueOf(email));
        if (password != null) {
            Log.d("checkUserExists", "Password is NOT null");
            password = characterEscape(password);
            if (email) {
                Log.d("checkUserExists", "Email is true, getting email and password");
                query = "SELECT user_email, user_pass FROM table_users";
            } else {
                Log.d("checkUserExists", "Email is false, getting username and password");
                query = "SELECT user_name, user_pass FROM table_users";
            }
        } else {
            Log.d("checkUserExists", "Password IS null");
            if (email) {
                Log.d("checkUserExists", "Email is true, getting email but NOT password");
                query = "SELECT user_email FROM table_users";
            } else {
                Log.d("checkUserExists", "Email is false, getting username but NOT password");
                query = "SELECT user_name FROM table_users";
            }
        }
        Log.d("checkUserExists", "query is: " + query);
        username = characterEscape(username);
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()){
            Log.d("checkUserExists", "Found users");
            do{
                if (password != null)
                    Log.d("checkUserExists", "Current cursor(0) is " + cursor.getString(0) + "\nCurrent cursor(1) is " + cursor.getString(1));
                else{
                    Log.d("checkUserExists", "Current cursor(0) is " + cursor.getString(0));
                }
                if (cursor.getString(0).equals(username)){
                    Log.d("checkUserExists", "username and cursor(0) match");
                    if (password != null){
                        Log.d("checkUserExists", "password is not null");
                        if (cursor.getString(1).equals(password)){
                            Log.d("checkUserExists", "password and cursor(1) match, returning true");
                            return true;
                        }
                        else{
                            Log.d("checkUserExists", "password and cursor(!) DO NOT match, returning false");
                            return false;
                        }
                    }
                    else{
                        Log.d("checkUserExists", "password is null and username matches, returning true");
                        return true;
                    }
                }
                else{
                    Log.d("checkUserExists", "Username and cursor(0) DO NOT match, continuing...");
                }
            }
            while(cursor.moveToNext());
            Log.d("checkUserExists", "Username not found in database, returning false");
            return false;
        }
        else{
            Log.d("checkUserExists", "No users found, returning false");
            return false;
        }
    }

    public String getUserID(String username){
        //This method gets the user's ID using their username. If they are not found it will instead return -1 as a String
        username = characterEscape(username);
        Log.d("getUserID", "Getting user's ID\nusername is " + username);
        Cursor cursor = db.rawQuery("SELECT user_id FROM table_users WHERE user_name = '" + username + "'", null);
        if (cursor.moveToFirst()){
            return cursor.getString(0);
        }
        else{
            return "-1";
        }
    }

    public String getUsername(String email){
        email = characterEscape(email);
        Log.d("getUsername", "Getting username. Email is " + email);
        Cursor cursor = db.rawQuery("SELECT user_name FROM table_users WHERE user_email = '" + email + "'", null);
        if (cursor.moveToFirst()){
            Log.d("getUsername", "Username is " + cursor.getString(0));
            return cursor.getString(0);
        }
        else{
            Log.d("getUsername", "Username not found");
            return "";
        }
    }

    public String getUsernameFromID(String uID){
        Log.d("getUsernameFromID", "Getting username from uID");
        Log.d("getUsernameFromID", "uID is " + uID);
        Cursor cursor = db.rawQuery("SELECT user_name FROM table_users WHERE user_id = "+ uID, null);
        cursor.moveToFirst();
        Log.d("getUsernameFromID", "cursor is " + cursor.getString(0));
        return cursor.getString(0);
    }

    public boolean modifyUserInfo(int columnNumber, String username, String newValue){
        /*This method modifies the users database info by replacing the old value with the newValue argument
        The argument "columnNumber" is used to decide which column in table_users is modified.
        */
        username = characterEscape(username);
        newValue = characterEscape(newValue);
        uID = getUserID(username);
        String table = "table_users";
        String whereClause = "user_id = " + uID;
        Log.d("modifyUserInfo", "uID is " + uID);
        Log.d("modifyUserInfo", "columnNumber is " + String.valueOf(columnNumber));
        Boolean newBool;
        ContentValues columnAndNewValue = new ContentValues();
        try {
            switch (columnNumber) {
                case 1: //Modifies username
                    Log.d("modifyUserInfo", "Modifying username to " + newValue);
                    columnAndNewValue.put("user_name", newValue);
                    break;
                case 2: //Modifies password
                    Log.d("modifyUserInfo", "Modifying password to " + newValue);
                    columnAndNewValue.put("user_pass", newValue);
                    break;
                case 3: //Modifies email
                    Log.d("modifyUserInfo", "Modifying email to " + newValue);
                    columnAndNewValue.put("user_email", newValue);
                    break;
                case 4: //Modifies bio
                    Log.d("modifyUserInfo", "Modifying bio to " + newValue);
                    columnAndNewValue.put("user_bio", newValue);
                case 5: //Modifies private setting
                    Log.d("modifyUserInfo", "Modifying private setting to " + newValue);
                    if (newValue.equals("true") || newValue.equals("True") || newValue.equals("1")) {
                        newBool = true;
                    } else if (newValue.equals("false") || newValue.equals("False") || newValue.equals("0")) {
                        newBool = false;
                    } else {
                        Log.d("modifyUserInfo", "Error: Boolean value not provided");
                        return false;
                    }
                    columnAndNewValue.put("user_private", newBool);
                    break;
                case 6: //Modifies ReceiveMessages setting
                    Log.d("modifyUserInfo", "Modifying RecMes setting to " + newValue);
                    if (newValue.equals("true") || newValue.equals("True") || newValue.equals("1")) {
                        newBool = true;
                    } else if (newValue.equals("false") || newValue.equals("False") || newValue.equals("0")) {
                        newBool = false;
                    } else {
                        Log.d("modifyUserInfo", "Error: Boolean value not provided");
                        return false;
                    }
                    columnAndNewValue.put("user_private", newBool);
                    break;
                case 7: //Change user PFP. TO BE DONE!
                    Log.d("modifyUserInfo", "Modifying user PFP to be done. Doing nothing for now");
                    return false;
                default:
                    Log.d("modifyUserInfo", "columnNumber not provided or is not between 1 and 7, doing nothing");
                    return false;
            }
            db.update(table, columnAndNewValue, whereClause, null);
            return true;
        }
        catch (Exception error){
            Log.d("modifyUserInfo", "Error when modifying user info");
            Log.d("modifyUserInfo", String.valueOf(error));
            return false;
        }
    }

    public String getUserEmail(String username){
        //This method gets the user's email from the database and returns it or, if it cannot find one, it returns an empty string.
        username = characterEscape(username);
        uID = getUserID(username);
        Log.d("getUserEmail", "uID is " + uID);
        Cursor cursor = db.rawQuery("SELECT user_email FROM table_users WHERE user_id = " + uID, null);
        if (cursor.moveToFirst()){
            Log.d("getUserEmail", "User exists, returning email");
            return cursor.getString(0);
        }
        else{
            Log.d("getUserEmail", "User not found, returning nothing");
            return "";
        }
    }

    public String getUserBio(String username){
        //This method gets the user's bio from the database and returns it or, if it cannot find one, it returns empty strings.
        username = characterEscape(username);
        uID = getUserID(username);
        Log.d("getUserBio", "uID is " + uID);
        Cursor cursor = db.rawQuery("SELECT user_bio FROM table_users WHERE user_id = " + uID, null);
        if (cursor.moveToFirst()){
            Log.d("getUserBio", "User exists, returning bio");
            return cursor.getString(0);
        }
        else{
            Log.d("getUserBio", "User not found, returning nothing");
            return "";
        }
    }

    public boolean getUserPrivateSetting(String username){
        //This method returns the current boolean state of the user's private setting.
        username = characterEscape(username);
        uID = getUserID(username);
        Log.d("getUserPrivateSetting", "");
        Cursor cursor = db.rawQuery("SELECT user_private FROM table_users WHERE user_id = " + uID, null);
        if (cursor.moveToFirst()){
            Log.d("getUserPrivateSetting", "User exists, translating value to bool");
            if (cursor.getString(0).equals("1")){
                Log.d("getUserPrivateSetting", "Value is 1, returning true");
                return true;
            }
            else if (cursor.getString(0).equals("0")){
                Log.d("getUserPrivateSetting", "Value is 0, returning false");
                return false;
            }
            else{
                Log.d("getUserPrivateSetting", "Value is not bool, returning false anyway");
                return false;
            }
        }
        else{
            Log.d("getUserPrivateSetting", "User not found, returning false anyway");
            return false;
        }
    }

    public boolean getUserRecMesSetting(String username){
        //This method returns the current boolean state of the user's RecMes setting.
        username = characterEscape(username);
        uID = getUserID(username);
        Log.d("getUserRecMesSetting", "uID is " + uID);
        Cursor cursor = db.rawQuery("SELECT user_recmes FROM table_users WHERE user_id = "+ uID, null);
        if (cursor.moveToFirst()){
            Log.d("getUserRecMesSetting", "User exists, translating value to bool");
            if (cursor.getString(0).equals("1")){
                Log.d("getUserRecMesSetting", "Value is 1, returning true");
                return true;
            }
            else if (cursor.getString(0).equals("0")){
                Log.d("getUserRecMesSetting", "Value is 0, returning false");
                return false;
            }
            else{
                Log.d("getUserRecMesSetting", "Value is not bool, returning false anyway");
                return false;
            }
        }
        else{
            Log.d("getUserRecMesSetting", "User not found, returning false anyway");
            return false;
        }
    }

    public String getUserPFP(String username){
        //Gets the user's profile picture
        username = characterEscape(username);
        uID = getUserID(username);
        Log.d("getUserPFP", "");
        Cursor cursor = db.rawQuery("SELECT user_pfp_file FROM table_users WHERE user_id = " + uID, null);
        if (cursor.moveToFirst()){
            Log.d("getUserPFP", "User exists, returning PFP");
            return cursor.getString(0);
        }
        else{
            Log.d("getUserPFP", "User not found, returning nothing");
            return "";
        }
    }

    public boolean deleteUserAccount(String username){
        //Deletes the users.
        username = characterEscape(username);
        uID = getUserID(username);
        Log.d("deleteUserAccount", "uID is " + uID);
        try{
            if (checkUserLinkedToVideos(username)){
                Log.d("deleteUserAccount", "User is linked to videos, deleting");
                deleteUserVideos(username);
            }
            if (checkUserLinkedToUsers(username, false)){
                Log.d("deleteUserAccount", "Users is subscribed to users, deleting");
                db.delete("link_users", "user_id = " + getUserID(username), null);
            }
            if (checkUserLinkedToUsers(username, true)){
                Log.d("deleteUserAccount", "User has subscribers, deleting");
                db.delete("link_users", "sub_id = " + getUserID(username), null);
            }
            if (checkUserLinkedToMessages(username)){
                Log.d("deleteUserAccount", "User has messages, deleting");
                db.delete("link_message_user", "user_id = " + getUserID(username), null);
            }
            Log.d("deleteUserAccount", "Deleting user");
            db.delete("table_users", "user_id = " + uID, null);
            return true;
        }
        catch (Exception error){
            Log.d("deleteUserAccount", "Error while deleting user's account");
            Log.d("deleteUserAccount", String.valueOf(error));
            return false;
        }
    }

    //The following methods relate to videos
    public boolean checkUserLinkedToVideos(String username){
        //Checks if a users is linked to ANY videos and returns true if they are.
        username = characterEscape(username);
        uID = getUserID(username);
        Cursor cursor = db.rawQuery("SELECT user_id FROM link_video_user WHERE user_id = " + uID, null);
        if (cursor.getCount() > 0){
            Log.d("checkUserLinkedToVideos", "User has videos");
            return true;
        }
        else{
            Log.d("checkUserLinkedToVideos", "User does not have videos");
            return false;
        }
    }

    public boolean checkUserLinkedToMessages(String username){
        Log.d("checkUserLinkedToMessages", "Checking if user has link to messages");
        Log.d("checkUserLinkedToMessages", "username is " + username);
        uID = getUserID(username);
        Log.d("checkUserLinkedToMessages", "uID is " + uID);
        Cursor cursor = db.rawQuery("SELECT user_id FROM link_message_user WHERE user_id = " + uID, null);
        if (cursor.getCount() > 0){
            Log.d("checkUserLinkedToMessages", "User has link to messages, returning true");
            return true;
        }
        else{
            Log.d("checkUserLinkedToMessages", "User has NO link to messages, returning false");
            return false;
        }
    }

    public boolean checkUserLinkedToUsers(String username, boolean subscriber){
        Log.d("checkUserLinkedToUsers", "Checking if user has link to users");
        Log.d("checkUserLinkedToUsers", "username is " + username);
        uID = getUserID(username);
        Log.d("checkUserLinkedToUsers", "uID is " + uID);
        if (subscriber) {
            Log.d("checkUserLinkedToUsers", "User in this context is a subscriber");
            Cursor cursor = db.rawQuery("SELECT user_id FROM link_users WHERE user_id = " + uID, null);
            if (cursor.getCount() > 0) {
                Log.d("checkUserLinkedToUsers", "User is linked to another user as a subscriber, returning true");
                return true;
            }
            else{
                Log.d("checkUserLinkedToUsers", "User is NOT linked to another user as a subscriber, returning false");
                return false;
            }
        }
        else{
            Log.d("checkUserLinkedToUsers", "User in this context is who people are subscribed to");
            Cursor cursor = db.rawQuery("SELECT sub_id FROM link_users WHERE user_id = " + uID, null);
            if (cursor.getCount() > 0){
                Log.d("checkUserLinkedToUsers", "User has subscribers, returning true");
                return true;
            }
            else{
                Log.d("checkUserLinkedToUsers", "User has NO subscribers, returning false");
                return false;
            }
        }
    }

    public String[] listVideos(){
        //This should be made to be random!
        Log.d("listVideos", "Listing videos");
        Cursor cursor = db.rawQuery("SELECT * FROM table_videos", null);
        String[] returnValues = new String[cursor.getCount()];
        if (cursor.moveToFirst()){
            Log.d("listVideos", "Videos found");
            int x = 0;
            do{
                Log.d("listVideos", "x is " + String.valueOf(x));
                Log.d("listVideos", "Current cursor is:\n" + cursor.getString(0) + "/" + cursor.getString(1) + "/" + cursor.getString(2) + "/" + cursor.getString(3) + "/" + cursor.getString(4) + "/" + cursor.getString(5) + "/" + cursor.getString(6) + "/" + cursor.getString(7) + "/" + cursor.getString(8));
                returnValues[x] = cursor.getString(0) + "/" + cursor.getString(1) + "/" + cursor.getString(2) + "/" + cursor.getString(3) + "/" + cursor.getString(4) + "/" + cursor.getString(5) + "/" + cursor.getString(6) + "/" + cursor.getString(7) + "/" + cursor.getString(8);
                x++;
            }
            while (x <= cursor.getCount() && cursor.moveToNext());
            Log.d("listVideos", "Got all videos, returning");
            return returnValues;
        }
        else{
            Log.d("listVideos", "No videos found!");
            return returnValues;
        }
    }

    public String getIndividualVideoOwner(String vID){
        Log.d("getIndividualVideoOwner", "Getting video owner");
        Log.d("getIndividualVideoOwner", "vID is " + uID);
        Cursor cursor = db.rawQuery("SELECT user_id FROM link_video_user WHERE video_id = " + vID, null);
        if (cursor.moveToFirst()){
            Log.d("getIndividualVideoOwner", "Current cursor is " + cursor.getString(0));
            return getUsernameFromID(cursor.getString(0));
        }
        else{
            return "";
        }
    }

    public String getIndividualVideoInfo(String vID){
        //To Do: Get username of video owner.
        Log.d("getIndividualVideoInfo", "Getting video's information");
        Log.d("getIndividualVideoInfo", "vID is " + vID);
        Cursor cursor = db.rawQuery("SELECT video_desc, video_date, video_views, video_likes, video_dislikes FROM table_videos WHERE video_id = " + vID, null);
        if (cursor.moveToFirst()){
            Log.d("getIndividualVideoInfo", "Video By: " + getIndividualVideoOwner(vID) + "\n\nDescription: " + cursor.getString(0) + "\n\nPosted On: " + cursor.getString(1) + "\nViews: " + cursor.getString(2) + "\nLikes: " + cursor.getString(3) + "\nDislikes" + cursor.getString(4));
            return "Video By: " + getIndividualVideoOwner(vID) + "\n\nDescription: " + cursor.getString(0) + "\n\nPosted On: " + cursor.getString(1) + "\nViews: " + cursor.getString(2) + "\nLikes: " + cursor.getString(3) + "\nDislikes" + cursor.getString(4);
        }
        else{
            Log.d("getIndividualVideoInfo", "No video information found! Returning nothing");
            return "";
        }
    }

    public String getIndividualVideoName(String vID){
        Log.d("getIndividualVideoName", "Getting video's name");
        Log.d("getIndividualVideoName", "vID is " + vID);
        Cursor cursor = db.rawQuery("SELECT video_name FROM table_videos WHERE video_id = " + vID, null);
        if (cursor.moveToFirst()){
            Log.d("getIndividualVideoName", "Cursor is " + cursor.getString(0));
            return cursor.getString(0);
        }
        else{
            Log.d("getIndividualVideoName", "Video name not found! Returning nothing");
            return "";
        }
    }

    public boolean increaseVideoViewCount(String vID){
        Log.d("increaseVideoViewCount", "Increasing view count of video");
        Log.d("increaseVideoViewCount", "vID is " + vID);
        try{
            Cursor cursor = db.rawQuery("SELECT video_views FROM table_videos WHERE video_id = " + vID, null);
            cursor.moveToFirst();
            Log.d("increaseVideoViewCount", "Old view count is " + cursor.getString(0));
            int viewCount = Integer.parseInt(cursor.getString(0));
            viewCount++;
            Log.d("increaseVideoViewCount", "New view count is " + String.valueOf(viewCount));
            ContentValues newViewCount = new ContentValues();
            newViewCount.put("video_views", viewCount);
            db.update("table_videos", newViewCount, "video_id = " + vID, null);
            return true;
        }
        catch (Exception error){
            Log.d("increaseVideoViewCount", "Updating of view count failed");
            Log.d("increaseVideoViewCount", String.valueOf(error));
            return false;
        }
    }

    public String[] videoSearch(String search){
        Log.d("videoSearch", "Searching for videos");
        search = characterEscape(search);
        Log.d("videoSearch", "search is " + search);
        String query = "SELECT * FROM table_videos WHERE video_name LIKE '%" + search + "%'";
        Log.d("videoSearch", "query is " + query);
        Cursor cursor = db.rawQuery(query, null);
        String[] returnValues = new String[cursor.getCount()];
        if (cursor.moveToFirst()){
            Log.d("videoSearch", "Videos found");
            int x = 0; //To do: GET ALL VIDEO DATA!
            int y = 0;
            do{
                Log.d("videoSearch", "x is " + String.valueOf(x));
                do{
                    Log.d("videoSearch", "y is " + String.valueOf(y));
                    if (returnValues[x] == null){
                        Log.d("videoSearch", "returnValues[" + String.valueOf(x) + "] is null");
                        returnValues[x] = cursor.getString(y);
                    }
                    else{
                        Log.d("videoSearch", "Adding to returnValues[" + String.valueOf(x) + "]");
                        Log.d("videoSearch", "Current column is " + cursor.getString(y));
                        returnValues[x] = returnValues[x] + "/" + cursor.getString(y);
                    }
                    y++;
                }
                while (y < cursor.getColumnCount());
                //Log.d("videoSearch", "Current cursor is " + cursor.getString(0) + "/" + cursor.getString(1) + "/" + cursor.getString(2) + "/" + cursor.getString(3) + "/" + cursor.getString(4) + "/" + cursor.getString(5) + "/" + cursor.getString(6) + "/" + cursor.get);
                Log.d("videoSearch", "returnValues[" + String.valueOf(x) + "] is " + returnValues[x]);
                y = 0;
                x++;
            }
            while (x <= cursor.getCount() && cursor.moveToNext());
        }
        return returnValues;
    }

    public boolean deleteUserVideos(String username){
        Log.d("deleteUserVideos", "Deleting videos by user");
        uID = getUserID(username);
        Log.d("deleteUserVideos", "uID is " + uID);
        Cursor cursor = db.rawQuery("SELECT video_id FROM link_video_user WHERE user_id = " + uID, null);
        String[] userVideoIDs = new String[cursor.getCount()];
        Log.d("deleteUserVideos", "Cursor count is " + String.valueOf(cursor.getCount()));
        if (cursor.moveToFirst()) {
            Log.d("deleteUserVideos", "Videos by user found");
            db.delete("link_video_user", "user_id = " + uID, null);
            int x = 0;
            do {
                Log.d("deleteUserVideos", "x is " + String.valueOf(x));
                Log.d("deleteUserVideos", "Current cursor is " + cursor.getString(0));
                userVideoIDs[x] = cursor.getString(0);
                x++;
            }
            while (x <= cursor.getCount() && cursor.moveToNext());
            Log.d("deleteUserVideos", "Retrieved video IDs, deleting...");
            x = 0;
            do{
                Log.d("deleteUserVideos", "x is " + String.valueOf(x));
                db.delete("table_videos", "video_id = " + userVideoIDs[x], null);
            }
            while (x <= userVideoIDs.length);
            Log.d("deleteUserVideos", "Link and videos deleted, returning true");
            return true;
        }
        else{
            Log.d("deleteUserVideos", "No videos by user found, returning false");
            return false;
        }
    }

    public void populateDB(){
        if (!checkUserExists("testUser", null, false)) {
            ContentValues dbData = new ContentValues();
            dbData.put("user_name", "testUser");
            dbData.put("user_pass", "12345678");
            dbData.put("user_email", "example@example.com");
            dbData.put("user_bio", "lol no");
            dbData.put("user_private", false);
            dbData.put("user_recmes", false);
            dbData.put("user_pfp_file", "None");
            db.insert("table_users", null, dbData);
            dbData.put("user_name", "hatman26");
            dbData.put("user_pass", "123456");
            dbData.put("user_email", "hatman.j.martin@outlook.com");
            dbData.put("user_bio", "Shhh...");
            dbData.put("user_private", false);
            dbData.put("user_recmes", false);
            dbData.put("user_pfp_file", "None");
            db.insert("table_users", null, dbData);
        }
        db.delete("link_video_user", "1=1", null);
        db.delete("table_videos", "1=1", null);
        ContentValues videoData = new ContentValues();
        videoData.put("video_name", "Getting Stuck in INFRA!");
        videoData.put("video_desc", "I locked the door by cheating. This video is a lie.");
        videoData.put("video_thumb_file", "default");
        videoData.put("video_date", "31/05/22");
        videoData.put("video_views", 0);
        videoData.put("video_likes", 0);
        videoData.put("video_dislikes", 0);
        videoData.put("video_file", "default");
        db.insert("table_videos", null, videoData);
        videoData = null;
        videoData = new ContentValues();
        videoData.put("video_id", 1);
        videoData.put("user_id", 1);
        db.insert("link_video_user", null, videoData);
        videoData = null;
        videoData = new ContentValues();
        videoData.put("video_name", "Where Oven");
        videoData.put("video_desc", "I have absolutely no idea.");
        videoData.put("video_thumb_file", "default");
        videoData.put("video_date", "02/10/22");
        videoData.put("video_views", 0);
        videoData.put("video_likes", 0);
        videoData.put("video_dislikes", 0);
        videoData.put("video_file", "default");
        db.insert("table_videos", null, videoData);
        videoData = null;
        videoData = new ContentValues();
        videoData.put("video_id", 2);
        videoData.put("user_id", 1);
        db.insert("link_video_user", null, videoData);
        videoData = null;
        videoData = new ContentValues();
        videoData.put("video_name", "Dialogue Test");
        videoData.put("video_desc", "GMS IS HARD!");
        videoData.put("video_thumb_file", "default");
        videoData.put("video_date", "31/08/22");
        videoData.put("video_views", 0);
        videoData.put("video_likes", 0);
        videoData.put("video_dislikes", 0);
        videoData.put("video_file", "default");
        db.insert("table_videos", null, videoData);
        videoData = null;
        videoData = new ContentValues();
        videoData.put("video_id", 3);
        videoData.put("user_id", 1);
        db.insert("link_video_user", null, videoData);
        videoData = null;
        videoData = new ContentValues();
        videoData.put("video_name", "NFS AI");
        videoData.put("video_desc", "Gotta love the AI of NFS Underground!");
        videoData.put("video_thumb_file", "default");
        videoData.put("video_date", "12/05/22");
        videoData.put("video_views", 0);
        videoData.put("video_likes", 0);
        videoData.put("video_dislikes", 0);
        videoData.put("video_file", "default");
        db.insert("table_videos", null, videoData);
        videoData = null;
        videoData = new ContentValues();
        videoData.put("video_id", 4);
        videoData.put("user_id", 1);
        db.insert("link_video_user", null, videoData);
        videoData = null;
        videoData = new ContentValues();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS link_video_user");
        db.execSQL("DROP TABLE IF EXISTS link_message_user");
        db.execSQL("DROP TABLE IF EXISTS table_users");
        db.execSQL("DROP TABLE IF EXISTS table_videos");
        db.execSQL("DROP TABLE IF EXISTS table_messages");
        this.onCreate(db);
    }
}
